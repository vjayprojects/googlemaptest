<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Bunnings Coburg Gaffney Street</name>
   <tag></tag>
   <elementGuidId>9fe73317-2b29-4f7d-9a55-f9e20f0314c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#sbse139 > div.sbqs_c > div.ZHeE1b.ZHeE1b-tPcied-dkl3Ye</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/jsl/div[3]/div[10]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[5]/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jstcache</name>
      <type>Main</type>
      <value>135</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ZHeE1b ZHeE1b-tPcied-dkl3Ye</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsan</name>
      <type>Main</type>
      <value>7.ZHeE1b,7.ZHeE1b-tPcied-dkl3Ye</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> nearby along the routeAdBunnings Coburg Gaffney Street, Coburg VIC, AustraliaSet locationEditAd</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sbse139&quot;)/div[@class=&quot;sbqs_c&quot;]/div[@class=&quot;ZHeE1b ZHeE1b-tPcied-dkl3Ye&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='sbse139']/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ad'])[8]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Burwood Road, Hawthorn VIC, Australia'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
