<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_64-96 Gaffney St, Coburg VIC 3058, Australia</name>
   <tag></tag>
   <elementGuidId>3bb84b90-0ea6-4596-bc3f-6f6deacf0cee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.QSFF4-text.gm2-body-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/jsl/div[3]/div[10]/div[8]/div/div[1]/div/div/div[9]/div[1]/button/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jstcache</name>
      <type>Main</type>
      <value>1149</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>QSFF4-text gm2-body-2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsan</name>
      <type>Main</type>
      <value>7.QSFF4-text,7.gm2-body-2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>64-96 Gaffney St, Coburg VIC 3058, Australia</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pane&quot;)/div[@class=&quot;widget-pane widget-pane-visible&quot;]/div[@class=&quot;widget-pane-content cYB2Ge-oHo7ed&quot;]/div[@class=&quot;widget-pane-content-holder&quot;]/div[@class=&quot;section-layout siAUzd-neVct-H9tDt&quot;]/div[@class=&quot;section-layout&quot;]/div[@class=&quot;RcCsl dqIYcf-RWgCYc-text w4vB1d C9yzub-TSZdd-on-hover-YuD1xf AG25L wL8d1-SDJVOb&quot;]/button[@class=&quot;CsEnBe&quot;]/div[@class=&quot;AeaXub&quot;]/div[@class=&quot;rogA2c&quot;]/div[@class=&quot;QSFF4-text gm2-body-2&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='pane']/div/div/div/div/div[9]/div/button/div/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='·'])[5]/following::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='·'])[4]/following::div[14]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Floor G · Lincoln Mills Homemaker Centre'])[1]/preceding::div[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open now:'])[1]/preceding::div[27]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='64-96 Gaffney St, Coburg VIC 3058, Australia']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
