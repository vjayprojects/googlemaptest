import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.google.com/maps/@-37.9074157,145.0445587,11.94z')

WebUI.setText(findTestObject('Google_Maps/Search_Field'), 'Bunnings')

WebUI.delay(2)

WebUI.click(findTestObject('Google_Maps/Bunnings Coburg Gaffney Street'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Google_Maps/div_64-96 Gaffney St, Coburg VIC 3058, Australia'), '64-96 Gaffney St, Coburg VIC 3058, Australia')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('Google_Maps/Buttons/div_Directions'))

WebUI.verifyElementVisible(findTestObject('Google_Maps/Buttons/div_Nearby'))

WebUI.verifyElementVisible(findTestObject('Google_Maps/Buttons/div_Save'))

WebUI.verifyElementVisible(findTestObject('Google_Maps/Buttons/div_Send to your phone'))

WebUI.verifyElementVisible(findTestObject('Google_Maps/Buttons/div_Share'))

WebUI.delay(3)

